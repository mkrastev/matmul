#!/bin/bash
CC=clang++
TARGET=test_cl
SOURCE=(
	main.cpp
	cl_util.cpp
	cl_wrap.cpp
)
CXXFLAGS=(
	-Wno-logical-op-parentheses
)

if [[ $1 == "debug" ]]; then
	CXXFLAGS+=(
		-Wall
		-O0
		-g
		-DDEBUG
	)
else
	CXXFLAGS+=(
		-funroll-loops
		-O3
		-DNDEBUG
	)
fi

if [[ `uname` == "Darwin" ]]; then
	LINKFLAGS=(
		-framework OpenCL
	)
else
	CXXFLAGS+=(
		-I.
	)
	LINKFLAGS=(
		-lOpenCL
		-ldl
		-lrt
	)
fi
BUILD_CMD=$CC" -o "$TARGET" "${CXXFLAGS[@]}" "${SOURCE[@]}" "${LINKFLAGS[@]}
echo $BUILD_CMD
$BUILD_CMD
