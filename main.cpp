#ifdef __APPLE__
	#include <OpenCL/cl.h>
#else
	#include <CL/cl.h>
#endif	
#if CL_VERSION_1_2 == 0
	#error required CL_VERSION_1_2 to compile
#endif
#include <iostream>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "cl_util.hpp"
#include "cl_wrap.hpp"
#include "scoped.hpp"
#include "timer.h"

static const char arg_prefix[] = "-";
static const char arg_report_caps[] = "report_caps";
static const char arg_discard_platform_version[] = "discard_platform_version";
static const char arg_discard_device_version[] = "discard_device_version";
static const char arg_platform[] = "platform";
static const char arg_device[] = "device";
static const char arg_use_images[] = "use_images";
static const char arg_use_lds_a[] = "use_lds_a";
static const char arg_use_lds_b[] = "use_lds_b";
static const char arg_use_explicit_mad[] = "use_explicit_mad";
static const char arg_entire_mtx[] = "entire_matrix";
static const char arg_aux_lat[] = "aux_latency_factor";

namespace testbed
{

template <>
class scoped_functor< cl_context >
{
public:
	void operator()(cl_context* arg)
	{
		assert(0 != arg);
		clReleaseContext(*arg);
	}
};

template <>
class scoped_functor< cl_command_queue >
{
public:
	void operator()(cl_command_queue* arg)
	{
		assert(0 != arg);
		clReleaseCommandQueue(*arg);
	}
};

template <>
class scoped_functor< cl_mem >
{
public:
	void operator()(cl_mem* arg)
	{
		assert(0 != arg);
		clReleaseMemObject(*arg);
	}
};

template <>
class scoped_functor< cl_program >
{
public:
	void operator()(cl_program* arg)
	{
		assert(0 != arg);
		clReleaseProgram(*arg);
	}
};

template <>
class scoped_functor< cl_kernel >
{
public:
	void operator ()(cl_kernel* arg)
	{
		assert(0 != arg);
		clReleaseKernel(*arg);
	}
};

template < typename T >
class generic_free
{
public:
	void operator()(T* arg)
	{
		assert(0 != arg);
		std::free(arg);
	}
};

} // namespace testbed

using testbed::scoped_ptr;
using testbed::scoped_functor;
using testbed::generic_free;

using clutil::ocl_ver;
using clutil::cldevice_version;
using clutil::cldevice_type_single_string;
using clutil::reportCLError;
using clutil::reportCLCaps;
using clwrap::init;
using clwrap::wrap_clCreateImage;

enum KERNEL_PARAM_TYPE
{
	PARAM_TYPE_BUFFER = 0,
	PARAM_TYPE_IMAGE,
	PARAM_TYPE_LDS_A, // LDS-cached buffer, ver A
	PARAM_TYPE_LDS_B  // LDS-cached buffer, ver B
};

enum FLAG_TYPE
{
	FLAG_EXPLICIT_MADD            = 1 << 0,
	FLAG_ENTIRE_MATRIX            = 1 << 1,
	FLAG_REPORT_CAPS              = 1 << 2,
	FLAG_DISCARD_PLATFORM_VERSION = 1 << 3,
	FLAG_DISCARD_DEVICE_VERSION   = 1 << 4
};

struct Param
{
	uint32_t flags;
	uint32_t latency_hiding_factor;
	uint32_t platform_idx;
	uint32_t device_idx;
	KERNEL_PARAM_TYPE param_type;
};

static int
parse_cli(
	const int argc,
	char** const argv,
	Param& param)
{
	const unsigned prefix_len = std::strlen(arg_prefix);
	bool success = true;

	for (int i = 1; i < argc && success; ++i)
	{
		if (std::strncmp(argv[i], arg_prefix, prefix_len))
		{
			success = false;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_report_caps))
		{
			param.flags |= FLAG_REPORT_CAPS;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_platform_version))
		{
			param.flags |= FLAG_DISCARD_PLATFORM_VERSION;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_discard_device_version))
		{
			param.flags |= FLAG_DISCARD_DEVICE_VERSION;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_platform))
		{
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.platform_idx))
				success = false;

			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_device))
		{
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.device_idx))
				success = false;

			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_use_images) && PARAM_TYPE_BUFFER == param.param_type)
		{
			param.param_type = PARAM_TYPE_IMAGE;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_use_lds_a) && PARAM_TYPE_BUFFER == param.param_type)
		{
			param.param_type = PARAM_TYPE_LDS_A;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_use_lds_b) && PARAM_TYPE_BUFFER == param.param_type)
		{
			param.param_type = PARAM_TYPE_LDS_B;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_use_explicit_mad))
		{
			param.flags |= FLAG_EXPLICIT_MADD;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_entire_mtx) && PARAM_TYPE_LDS_A > param.param_type)
		{
			param.flags |= FLAG_ENTIRE_MATRIX;
			continue;
		}

		if (!std::strcmp(argv[i] + prefix_len, arg_aux_lat))
		{
			if (++i == argc || 1 != sscanf(argv[i], "%u", &param.latency_hiding_factor))
				success = false;

			continue;
		}

		success = false;
	}

	if (!success)
	{
		std::cerr << "usage: " << argv[0] << " [<option> ...]\n"
			"options (multiple args to an option must constitute a single string, eg. -foo \"a b c\"):\n"
			"\t" << arg_prefix << arg_report_caps <<
			"\t\t\t: report CL capabilities\n"
			"\t" << arg_prefix << arg_discard_platform_version <<
			"\t: discard advertised platform version when collecting platform info\n"
			"\t" << arg_prefix << arg_discard_device_version <<
			"\t\t: discard advertised device version when collectin device info\n"
			"\t" << arg_prefix << arg_platform <<
			" <index>\t\t: use platform of specified index\n"
			"\t" << arg_prefix << arg_device <<
			" <index>\t\t\t: use device of specified index\n"
			"\t" << arg_prefix << arg_use_lds_a <<
			"\t\t\t: use LDS as transient storage for source arguments passed in buffers, ver. A\n"
			"\t" << arg_prefix << arg_use_lds_b <<
			"\t\t\t: use LDS as transient storage for source arguments passed in buffers, ver. B\n"
			"\t" << arg_prefix << arg_use_images <<
			"\t\t\t: use images instead of buffers as source arguments\n"
			"\t" << arg_prefix << arg_use_explicit_mad <<
			"\t\t: use explicit MADD function\n"
			"\t" << arg_prefix << arg_aux_lat <<
			" <factor>\t: auxiliary latency-hiding workgroup-size factor (default is 1)\n"
			"\t" << arg_prefix << arg_entire_mtx <<
			"\t\t\t: let threads compute entire matrices rather than individual rows; incompatible with LDS transient storage\n" << std::endl;

		return 1;
	}

	return 0;
}


int
main(
	int argc,
	char** argv)
{
	Param param =
	{
		0,                // uint32_t flags;
		1,                // uint32_t latency_hiding_factor;
		0,                // uint32_t platform_idx;
		-1U,              // uint32_t device_idx;
		PARAM_TYPE_BUFFER // KERNEL_PARAM_TYPE param_type;
	};

	const int result_cli = parse_cli(argc, argv, param);

	if (0 != result_cli)
		return result_cli;

	const bool report_caps = param.flags & FLAG_REPORT_CAPS;
	const bool discard_platform_version = param.flags & FLAG_DISCARD_PLATFORM_VERSION;
	const bool discard_device_version = param.flags & FLAG_DISCARD_DEVICE_VERSION;
	const bool use_explicit_mad = param.flags & FLAG_EXPLICIT_MADD;
	const bool entire = param.flags & FLAG_ENTIRE_MATRIX;

	const size_t latency_hiding_factor = param.latency_hiding_factor;
	const unsigned platform_idx = param.platform_idx;
	unsigned device_idx = param.device_idx;
	const KERNEL_PARAM_TYPE param_type = param.param_type;

	if (report_caps)
	{
		const int result_caps = reportCLCaps(
			discard_platform_version,
			discard_device_version);

		if (0 != result_caps)
			return result_caps;
	}

	cl_uint num_platforms = 0;
	cl_int success = clGetPlatformIDs(num_platforms, 0, &num_platforms);

	if (reportCLError(success))
	{
		std::cerr << "failure at clGetPlatformIDs; terminate" << std::endl;
		return -1;
	}

	if (platform_idx >= unsigned(num_platforms))
	{
		std::cerr << "requested platform index is out of bound" << std::endl;
		return 1;
	}

	const scoped_ptr< cl_platform_id, generic_free > platform(
		(cl_platform_id*) std::malloc(sizeof(cl_platform_id) * num_platforms));

	success = clGetPlatformIDs(num_platforms, platform(), 0);

	if (reportCLError(success))
	{
		std::cerr << "failure at clGetPlatformIDs; terminate" << std::endl;
		return -1;
	}

	scoped_ptr< cl_device_id, generic_free > device;

	if (-1U != device_idx)
	{
		cl_uint num_devices = 0;

		success = clGetDeviceIDs(platform()[platform_idx],
			CL_DEVICE_TYPE_ALL, num_devices, 0, &num_devices);

		if (reportCLError(success))
		{
			std::cerr << "failure at clGetDeviceIDs; terminate" << std::endl;
			return -1;
		}

		if (device_idx >= unsigned(num_devices))
		{
			std::cerr << "requested device index is out of bound" << std::endl;
			return 1;
		}

		scoped_ptr< cl_device_id, generic_free > proto_device(
			(cl_device_id*) std::malloc(sizeof(cl_device_id) * num_devices));

		success = clGetDeviceIDs(platform()[platform_idx],
			CL_DEVICE_TYPE_ALL, num_devices, proto_device(), 0);

		if (reportCLError(success))
		{
			std::cerr << "failure at clGetDeviceIDs; terminate" << std::endl;
			return -1;
		}

		device.swap(proto_device);
	}
	else
	{
		const cl_device_type devtype = CL_DEVICE_TYPE_GPU;

		scoped_ptr< cl_device_id, generic_free > proto_device(
			(cl_device_id*) std::malloc(sizeof(cl_device_id)));

		success = clGetDeviceIDs(platform()[platform_idx],
			devtype, 1, proto_device(), 0);

		if (reportCLError(success))
		{
			std::cerr << "error getting device of type " <<
				cldevice_type_single_string(devtype) << std::endl;
			return -1;
		}

		device_idx = 0;
		device.swap(proto_device);
	}

	ocl_ver platform_version, device_version;

	if (!clplatform_version(platform()[platform_idx], platform_version) ||
		!cldevice_version(device()[device_idx], device_version))
	{
		std::cerr << "error getting platform/device version" << std::endl;
		return -1;
	}

	if (!init(platform_version))
	{
		std::cerr << "error initializing API wrappers" << std::endl;
		return -1;
	}

	const cl_context_properties prop[] =
	{
		CL_CONTEXT_PLATFORM, cl_context_properties(platform()[platform_idx]),
		0
	};

	cl_context context = clCreateContext(prop, 1, device() + device_idx, 0, 0, &success);

	if (reportCLError(success))
	{
		std::cerr << "error creating context" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_context, scoped_functor > release_context(&context);

	cl_command_queue queue = clCreateCommandQueue(context,
		device()[device_idx], CL_QUEUE_PROFILING_ENABLE, &success);

	if (reportCLError(success))
	{
		std::cerr << "error creating command queue" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_command_queue, scoped_functor > release_queue(&queue);

	const size_t image_w = 512; // don't go overboard - size of the io buffers is limited by CL_DEVICE_MAX_MEM_ALLOC_SIZE
	const size_t image_h = 512;
	assert(0 == (image_w & 3));

	if (image_w & 3)
	{
		std::cerr << "image width is not a multiple of 4; bailing out" << std::endl;
		return -1;
	}

	const size_t mem_size = image_w * image_h * sizeof(cl_float4);
	const size_t mat_count = mem_size / sizeof(cl_float16);

	if (0 == mat_count)
	{
		std::cerr << "not enough input data to form a matrix; bailing out" << std::endl;
		return 1;
	}

	const scoped_ptr< cl_float16, generic_free > identity_map(
		(cl_float16*) std::malloc(mem_size));

	if (0 == identity_map())
	{
		std::cerr << "error allocating identity_map" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_float16, generic_free > monotonic_map(
		(cl_float16*) std::malloc(mem_size));

	if (0 == monotonic_map())
	{
		std::cerr << "error allocating monotonic_map" << std::endl;
		return -1;
	}

	for (size_t i = 0; i < mat_count; ++i)
	{
		class cl_float16_init
		{
			cl_float16 m;

		public:
			cl_float16_init(
				float c0, float c1, float c2, float c3,
				float c4, float c5, float c6, float c7,
				float c8, float c9, float ca, float cb,
				float cc, float cd, float ce, float cf)
			{
				m.s0 = c0;
				m.s1 = c1;
				m.s2 = c2;
				m.s3 = c3;
				m.s4 = c4;
				m.s5 = c5;
				m.s6 = c6;
				m.s7 = c7;
				m.s8 = c8;
				m.s9 = c9;
				m.sa = ca;
				m.sb = cb;
				m.sc = cc;
				m.sd = cd;
				m.se = ce;
				m.sf = cf;
			}

			operator cl_float16() const
			{
				return m;
			}
		};

		identity_map()[i] = cl_float16_init(
			1.f, 0.f, 0.f, 0.f,
			0.f, 1.f, 0.f, 0.f,
			0.f, 0.f, 1.f, 0.f,
			0.f, 0.f, 0.f, 1.f);

		monotonic_map()[i] = cl_float16_init(
			float(i + 1), 0.f, 0.f, 0.f,
			0.f, float(i + 1), 0.f, 0.f,
			0.f, 0.f, float(i + 1), 0.f,
			0.f, 0.f, 0.f, float(i + 1));
	}

	const cl_image_format src_image_format =
	{
		CL_RGBA,				// cl_channel_order image_channel_order;
		CL_FLOAT				// cl_channel_type image_channel_data_type;
	};

	const cl_image_desc src_image_desc =
	{
		CL_MEM_OBJECT_IMAGE2D,	// cl_mem_object_type image_type;
		image_w,				// size_t image_width;
		image_h,				// size_t image_height;
		size_t(0),				// size_t image_depth;
		size_t(0),				// size_t image_array_size;
		size_t(0),				// size_t image_row_pitch;
		size_t(0),				// size_t image_slice_pitch;
		cl_uint(0),				// cl_uint num_mip_levels;
		cl_uint(0),				// cl_uint num_samples;
		cl_mem(0)				// cl_mem buffer;
	};

	cl_mem_flags src_flags = CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR;

	if (device_version >= ocl_ver(1, 2))
		src_flags |= CL_MEM_HOST_NO_ACCESS;

	cl_mem src_a_d = 0;

	switch (param_type)
	{
	case PARAM_TYPE_BUFFER:
	case PARAM_TYPE_LDS_A:
	case PARAM_TYPE_LDS_B:
		src_a_d = clCreateBuffer(context,
			src_flags, mem_size, identity_map(), &success);
		break;

	case PARAM_TYPE_IMAGE:
		src_a_d = wrap_clCreateImage(context,
			src_flags, &src_image_format, &src_image_desc, identity_map(), &success);
		break;
	}

	if (reportCLError(success))
	{
		std::cerr << "error creating buffer for src_a" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_mem, scoped_functor > release_src_a(&src_a_d);

	cl_mem src_b_d = 0;

	switch (param_type)
	{
	case PARAM_TYPE_BUFFER:
	case PARAM_TYPE_LDS_A:
	case PARAM_TYPE_LDS_B:
		src_b_d = clCreateBuffer(context,
			src_flags, mem_size, monotonic_map(), &success);
		break;

	case PARAM_TYPE_IMAGE:
		src_b_d = wrap_clCreateImage(context,
			src_flags, &src_image_format, &src_image_desc, monotonic_map(), &success);
		break;
	}

	if (reportCLError(success))
	{
		std::cerr << "error creating buffer for src_b" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_mem, scoped_functor > release_src_b(&src_b_d);

	cl_mem_flags dst_flags = CL_MEM_WRITE_ONLY;

	if (device_version >= ocl_ver(1, 2))
		dst_flags |= CL_MEM_HOST_READ_ONLY;

	cl_mem dst_d = clCreateBuffer(context, dst_flags, mem_size, 0, &success);

	if (reportCLError(success))
	{
		std::cerr << "error creating buffer for result" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_mem, scoped_functor > release_dst(&dst_d);

	const char source_buffer[] =
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__global const float4* const src_a,\n"
		"	__global const float4* const src_b,\n"
		"	__global float4* const dst)\n"
		"{\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const float4 a = src_a[idx];\n"
		"	const size_t mat_idx = idx & ~3;\n"
		"	const float4 b0 = src_b[mat_idx + 0];\n"
		"	const float4 b1 = src_b[mat_idx + 1];\n"
		"	const float4 b2 = src_b[mat_idx + 2];\n"
		"	const float4 b3 = src_b[mat_idx + 3];\n";

	const char source_buffer_output[] =
		"	dst[idx] = row;\n"
		"}";

	const char source_image[] =
		"__constant sampler_t sampler_a = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;\n"
		"__constant sampler_t sampler_b = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;\n"
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__read_only image2d_t src_a,\n"
		"	__read_only image2d_t src_b,\n"
		"	__global float4* const dst)\n"
		"{\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const size_t idy = get_global_id(1);\n"
		"	const float4 a = read_imagef(src_a, sampler_a, (int2)(idx, idy));\n"
		"	const size_t mat_idx = idx & ~3;\n"
		"	const float4 b0 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 0, idy));\n"
		"	const float4 b1 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 1, idy));\n"
		"	const float4 b2 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 2, idy));\n"
		"	const float4 b3 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 3, idy));\n";

	const char source_image_output[] =
		"	dst[get_global_size(0) * idy + idx] = row;\n"
		"}";

	const char source_implicit_mad[] =
		"	const float4 row =\n"
		"		a.x * b0 +\n"
		"		a.y * b1 +\n"
		"		a.z * b2 +\n"
		"		a.w * b3;\n";

	const char source_explicit_mad[] =
		"	float4 row = a.x * b0;\n"
		"	row = mad((float4)a.y, b1, row);\n"
		"	row = mad((float4)a.z, b2, row);\n"
		"	row = mad((float4)a.w, b3, row);\n";

	const char source_lds_a[] =
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__global const float4* const src_a,\n"
		"	__global const float4* const src_b,\n"
		"	__global float4* const dst,\n"
		"	__local float4* const loc_b)\n"
		"{\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const size_t ldx = get_local_id(0);\n"
		"\n"
		"	const float4 a = src_a[idx];\n"
		"	loc_b[ldx] = src_b[idx];\n"
		"\n"
		"	mem_fence(CLK_LOCAL_MEM_FENCE);\n"
		"\n"
		"	const float4 b0 = loc_b[(ldx & ~3) + 0];\n"
		"	const float4 b1 = loc_b[(ldx & ~3) + 1];\n"
		"	const float4 b2 = loc_b[(ldx & ~3) + 2];\n"
		"	const float4 b3 = loc_b[(ldx & ~3) + 3];\n";

	const char source_lds_b[] =
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__global const float4* const src_a,\n"
		"	__global const float4* const src_b,\n"
		"	__global float4* const dst,\n"
		"	__local float4* const loc_b)\n"
		"{\n"
		"	const size_t local_size = get_local_size(0);\n"
		"	event_t done_copy = async_work_group_copy(loc_b, src_b + get_group_id(0) * local_size, local_size, 0);\n"
		"\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const size_t ldx = get_local_id(0);\n"
		"	const float4 a = src_a[idx];\n"
		"\n"
		"	wait_group_events(1, &done_copy);\n"
		"\n"
		"	const float4 b0 = loc_b[(ldx & ~3) + 0];\n"
		"	const float4 b1 = loc_b[(ldx & ~3) + 1];\n"
		"	const float4 b2 = loc_b[(ldx & ~3) + 2];\n"
		"	const float4 b3 = loc_b[(ldx & ~3) + 3];\n";

	const char source_buffer_entire[] =
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__global const float4* const src_a,\n"
		"	__global const float4* const src_b,\n"
		"	__global float4* const dst)\n"
		"{\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const size_t mat_idx = idx * 4;\n"
		"	const float4 b0 = src_b[mat_idx + 0];\n"
		"	const float4 b1 = src_b[mat_idx + 1];\n"
		"	const float4 b2 = src_b[mat_idx + 2];\n"
		"	const float4 b3 = src_b[mat_idx + 3];\n"
		"	const float4 a0 = src_a[mat_idx + 0];\n"
		"	const float4 a1 = src_a[mat_idx + 1];\n"
		"	const float4 a2 = src_a[mat_idx + 2];\n"
		"	const float4 a3 = src_a[mat_idx + 3];\n";

	const char source_buffer_output_entire[] =
		"	dst[mat_idx + 0] = row0;\n"
		"	dst[mat_idx + 1] = row1;\n"
		"	dst[mat_idx + 2] = row2;\n"
		"	dst[mat_idx + 3] = row3;\n"
		"}";

	const char source_image_entire[] =
		"__constant sampler_t sampler_a = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;\n"
		"__constant sampler_t sampler_b = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;\n"
		"__kernel __attribute__ ((vec_type_hint(float4)))\n"
		"void matmul(\n"
		"	__read_only image2d_t src_a,\n"
		"	__read_only image2d_t src_b,\n"
		"	__global float4* const dst)\n"
		"{\n"
		"	const size_t idx = get_global_id(0);\n"
		"	const size_t idy = get_global_id(1);\n"
		"	const size_t mat_idx = idx * 4;\n"
		"	const float4 b0 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 0, idy));\n"
		"	const float4 b1 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 1, idy));\n"
		"	const float4 b2 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 2, idy));\n"
		"	const float4 b3 = read_imagef(src_b, sampler_b, (int2)(mat_idx + 3, idy));\n"
		"	const float4 a0 = read_imagef(src_a, sampler_a, (int2)(mat_idx + 0, idy));\n"
		"	const float4 a1 = read_imagef(src_a, sampler_a, (int2)(mat_idx + 1, idy));\n"
		"	const float4 a2 = read_imagef(src_a, sampler_a, (int2)(mat_idx + 2, idy));\n"
		"	const float4 a3 = read_imagef(src_a, sampler_a, (int2)(mat_idx + 3, idy));\n";

	const char source_image_output_entire[] =
		"	dst[get_global_size(0) * 4 * idy + mat_idx + 0] = row0;\n"
		"	dst[get_global_size(0) * 4 * idy + mat_idx + 1] = row1;\n"
		"	dst[get_global_size(0) * 4 * idy + mat_idx + 2] = row2;\n"
		"	dst[get_global_size(0) * 4 * idy + mat_idx + 3] = row3;\n"
		"}";

	const char source_implicit_mad_entire[] =
		"	const float4 row0 =\n"
		"		a0.x * b0 +\n"
		"		a0.y * b1 +\n"
		"		a0.z * b2 +\n"
		"		a0.w * b3;\n"
		"	const float4 row1 =\n"
		"		a1.x * b0 +\n"
		"		a1.y * b1 +\n"
		"		a1.z * b2 +\n"
		"		a1.w * b3;\n"
		"	const float4 row2 =\n"
		"		a2.x * b0 +\n"
		"		a2.y * b1 +\n"
		"		a2.z * b2 +\n"
		"		a2.w * b3;\n"
		"	const float4 row3 =\n"
		"		a3.x * b0 +\n"
		"		a3.y * b1 +\n"
		"		a3.z * b2 +\n"
		"		a3.w * b3;\n";

	const char source_explicit_mad_entire[] =
		"	float4 row0 = a0.x * b0;\n"
		"	row0 = mad((float4)a0.y, b1, row0);\n"
		"	row0 = mad((float4)a0.z, b2, row0);\n"
		"	row0 = mad((float4)a0.w, b3, row0);\n"
		"	float4 row1 = a1.x * b0;\n"
		"	row1 = mad((float4)a1.y, b1, row1);\n"
		"	row1 = mad((float4)a1.z, b2, row1);\n"
		"	row1 = mad((float4)a1.w, b3, row1);\n"
		"	float4 row2 = a2.x * b0;\n"
		"	row2 = mad((float4)a2.y, b1, row2);\n"
		"	row2 = mad((float4)a2.z, b2, row2);\n"
		"	row2 = mad((float4)a2.w, b3, row2);\n"
		"	float4 row3 = a3.x * b0;\n"
		"	row3 = mad((float4)a3.y, b1, row3);\n"
		"	row3 = mad((float4)a3.z, b2, row3);\n"
		"	row3 = mad((float4)a3.w, b3, row3);\n";

	const char* source[] =
	{
		entire ? source_buffer_entire        : source_buffer,
		entire ? source_implicit_mad_entire  : source_implicit_mad,
		entire ? source_buffer_output_entire : source_buffer_output
	};

	switch (param_type)
	{
	case PARAM_TYPE_IMAGE:
		source[0] = entire ? source_image_entire        : source_image;
		source[2] = entire ? source_image_output_entire : source_image_output;
		break;

	case PARAM_TYPE_LDS_A:
		source[0] = source_lds_a;
		break;

	case PARAM_TYPE_LDS_B:
		source[0] = source_lds_b;
		break;
	}

	if (use_explicit_mad)
		source[1] = entire ? source_explicit_mad_entire : source_explicit_mad;

	cl_program program = clCreateProgramWithSource(context,
		sizeof(source) / sizeof(source[0]), source, 0, &success);

	if (reportCLError(success))
	{
		std::cerr << "error creating program with source" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_program, scoped_functor > release_program(&program);

	success = clBuildProgram(program, 1, device() + device_idx, "-cl-mad-enable", 0, 0);

	if (reportCLError(success) && CL_BUILD_PROGRAM_FAILURE != success)
	{
		std::cerr << "error building program" << std::endl;
		return -1;
	}

	cl_build_status build_status = CL_BUILD_NONE;
	success = clGetProgramBuildInfo(program,
		device()[device_idx], CL_PROGRAM_BUILD_STATUS, sizeof(build_status), &build_status, 0);

	if (reportCLError(success))
	{
		std::cerr << "error getting build info (build_status)" << std::endl;
		return -1;
	}

	if (CL_BUILD_SUCCESS != build_status)
	{
		size_t log_len = 0;
		success = clGetProgramBuildInfo(program,
			device()[device_idx], CL_PROGRAM_BUILD_LOG, 0, 0, &log_len);

		if (reportCLError(success))
		{
			std::cerr << "error getting build info (log_len)" << std::endl;
			return -1;
		}

		const scoped_ptr< char, generic_free > build_log(
			(char*) std::calloc(log_len, sizeof(char)));

		success = clGetProgramBuildInfo(program,
			device()[device_idx], CL_PROGRAM_BUILD_LOG, log_len, build_log(), 0);

		if (reportCLError(success))
		{
			std::cerr << "error getting build info (build_log)" << std::endl;
			return -1;
		}

		std::cerr << build_log() << std::endl;
		return -1;
	}

	cl_kernel kernel = clCreateKernel(program, "matmul", &success);

	if (reportCLError(success))
	{
		std::cerr << "error creating kernel" << std::endl;
		return -1;
	}

	const scoped_ptr< cl_kernel, scoped_functor > release_kernel(&kernel);

	size_t preferred_local_ws = 0;
	success = clGetKernelWorkGroupInfo(kernel,
		device()[device_idx], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
		sizeof(preferred_local_ws), &preferred_local_ws, 0);

	if (reportCLError(success))
	{
		std::cerr << "error getting kernel workgroup info" << std::endl;
		return -1;
	}

	success = clSetKernelArg(kernel, 0, sizeof(src_a_d), &src_a_d);

	if (reportCLError(success))
	{
		std::cerr << "error setting kernel arg 0" << std::endl;
		return -1;
	}

	success = clSetKernelArg(kernel, 1, sizeof(src_b_d), &src_b_d);

	if (reportCLError(success))
	{
		std::cerr << "error setting kernel arg 1" << std::endl;
		return -1;
	}

	success = clSetKernelArg(kernel, 2, sizeof(dst_d), &dst_d);

	if (reportCLError(success))
	{
		std::cerr << "error setting kernel arg 2" << std::endl;
		return -1;
	}

	const size_t total_local_ws = preferred_local_ws * latency_hiding_factor;

	if (0 != (total_local_ws & 3))
	{
		std::cerr << "workgroup size is not a multiple of 4; bailing out" << std::endl;
		return -1;
	}

	if (PARAM_TYPE_LDS_A <= param_type)
	{
		success = clSetKernelArg(kernel, 3, sizeof(cl_float4) * total_local_ws, 0); // one matrix row per thread

		if (reportCLError(success))
		{
			std::cerr << "error setting kernel arg 3" << std::endl;
			return -1;
		}
	}

	size_t local_ws[] = { preferred_local_ws, latency_hiding_factor };
	size_t global_ws[] = { entire ? image_w / 4 : image_w, image_h };
	cl_uint work_dim = 2;

	if (PARAM_TYPE_IMAGE != param_type)
	{
		local_ws[0] *= local_ws[1];
		local_ws[1] = 0;
		global_ws[0] *= global_ws[1];
		global_ws[1] = 0;
		work_dim = 1;
	}

	for (cl_uint i = 0; i < work_dim; ++i)
		if (0 != global_ws[i] % local_ws[i])
		{
			std::cerr << "ND range not a multiple of workgroup size; bailing out" << std::endl;
			return 1;
		}

	const uint64_t cpu_t0 = timer_ns();

	cl_event e_kernel;
	success = clEnqueueNDRangeKernel(queue, kernel, work_dim, 0, global_ws, local_ws, 0, 0, &e_kernel);

	if (reportCLError(success))
	{
		std::cerr << "error enqueuing kernel" << std::endl;
		return -1;
	}

	// explicitly wait for the kernel event; normally we would enqueue the result buffer readback here,
	// followed by the read of the event profiling info, but that does seem to produce abnormal varience
	// in the event profiling times on some stacks! so we currently read the profiling times as soon as
	// possible after the event occurrence, ergo the explicit wait.
	success = clWaitForEvents(1, &e_kernel);

	const uint64_t cpu_dt = timer_ns() - cpu_t0;

	if (reportCLError(success))
	{
		std::cerr << "error waiting for kernel event" << std::endl;
		return -1;
	}

	cl_ulong t0, t1;

	success = clGetEventProfilingInfo(
		e_kernel, CL_PROFILING_COMMAND_START, sizeof(t0), &t0, 0);

	if (reportCLError(success))
	{
		std::cerr << "error getting profiling info (command_start)" << std::endl;
		return -1;
	}

	success = clGetEventProfilingInfo(
		e_kernel, CL_PROFILING_COMMAND_END, sizeof(t1), &t1, 0);

	if (reportCLError(success))
	{
		std::cerr << "error getting profiling info (command_end)" << std::endl;
		return -1;
	}

	std::memset(identity_map(), 0, mem_size);

	success = clEnqueueReadBuffer(queue, dst_d, CL_TRUE, 0, mem_size, identity_map(), 1, &e_kernel, 0);

	if (reportCLError(success))
	{
		std::cerr << "error enqueuing buffer read" << std::endl;
		return -1;
	}

	std::cout <<
		"kernel preferred workgroup size multiple: " << preferred_local_ws << std::endl <<
		"global ws: " << global_ws[0] << ", " << global_ws[1] << std::endl <<
		"local ws: " << local_ws[0] << ", " << local_ws[1] << std::endl;

	size_t i = 0;
	for (; i < std::min(size_t(4), mat_count); ++i)
	{
		const cl_float16& e = identity_map()[i];
		std::cout <<
			e.s0 << '\t' << e.s1 << '\t' << e.s2 << '\t' << e.s3 << '\n' <<
			e.s4 << '\t' << e.s5 << '\t' << e.s6 << '\t' << e.s7 << '\n' <<
			e.s8 << '\t' << e.s9 << '\t' << e.sa << '\t' << e.sb << '\n' <<
			e.sc << '\t' << e.sd << '\t' << e.se << '\t' << e.sf << '\n' << std::endl;
	}

	bool ok = true;

	for (; i < mat_count && ok; ++i)
	{
		const cl_float16& e = identity_map()[i];

		for (size_t j = 0; j < 4; ++j) {
			const float major_diagonal = e.s[j * 5];
			if (major_diagonal != float(i + 1))
			{
				std::cerr << "error in result matrix " << i << ", row " << j << " (" << major_diagonal << ")" << std::endl;
				ok = false;
			}
		}
	}

	std::cout << "elapsed dev time (ns): " << t1 - t0 <<
		"\nelapsed cpu time (ns): " << cpu_dt << std::endl;

	return 0;
}
